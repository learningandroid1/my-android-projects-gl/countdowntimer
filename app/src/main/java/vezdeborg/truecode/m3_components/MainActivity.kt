package vezdeborg.truecode.m3_components

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.slider.Slider
import vezdeborg.truecode.m3_components.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var targetTime: Int = 10
    private lateinit var binding: ActivityMainBinding
    private var timer: CountDownTimer? = null
    private var isTimerSet: Boolean = false
    private lateinit var countText: TextView
    private lateinit var progressView: ProgressBar
    private lateinit var slider: Slider
    private lateinit var button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        countText = binding.countText
        progressView = binding.circleProgress
        slider = binding.slider
        button = binding.button

        isTimerSet = true
        updateUI()

        slider.addOnChangeListener { _, _, _ ->
            setTargetTime()
            isTimerSet = true
            updateUI()
        }

        button.setOnClickListener {
            if (isTimerSet) {
                isTimerSet = false
                startTimer(targetTime)
            } else {
                stopTimer()
            }
        }
    }

    private fun updateUI(){
        if (isTimerSet){
            countText.text = targetTime.toString()
            progressView.max = slider.value.toInt()
            progressView.progress = progressView.max
            slider.isEnabled = true
            button.text = "START"
        } else {
            countText.text = targetTime.toString()
            progressView.progress = targetTime
            slider.isEnabled = false
            button.text = "STOP"
        }
    }

    private fun startTimer(time: Int) {
        timer = object : CountDownTimer(time.toLong() * 1_000, 1000) {
            override fun onTick(timeM: Long) {
                targetTime -= 1
                updateUI()
            }

            override fun onFinish() {
                showFinishToast()
                timer = null
                setTargetTime()
                isTimerSet = true
                updateUI()
            }
        }.start()
    }
    private fun stopTimer(){
        showFinishToast()
        timer?.cancel()
        timer = null
        setTargetTime()
        isTimerSet = true
        updateUI()
    }

    private fun showFinishToast(){
        Toast.makeText(this, "Task finished", Toast.LENGTH_LONG).show()
    }

    private fun setTargetTime(){
        targetTime = slider.value.toInt()
    }
}